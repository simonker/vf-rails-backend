class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.boolean :is_admin
      t.string :password
      t.string :hashed_password
      t.string :salt
      t.string :address1
      t.string :address2
      t.string :address3
      t.string :postcode
      t.string :city
      t.string :phone_number

      t.timestamps null: false
    end
  end
end
