class AccessController < ApplicationController
          before_filter :logged_in_as_admin, :except => [:login, :attempt_login, :logout]

          def index
             redirect_to(:users => 'index')
          end

          def login
            # login form
          end


          def attempt_login
            authorized_user = User.authenticate(params[:email], params[:password])
            if authorized_user
              session[:user_id] = authorized_user.id
              if authorized_user.is_admin?
                session[:admin_rights] = true
              end
              session[:email] = authorized_user.email
              flash[:notice] = "You are now logged in."
              redirect_to(:controller => "users", :action => 'index', :id => session[:user_id])
            else
              flash[:notice] = "Invalid username/password combination."
              redirect_to(:controller => 'access', :action => 'login')
            end

          end

          def logout
            session[:user_id] = nil
            session[:email] = nil
            session[:admin_rights] = nil
            flash[:notice] = "You have been logged out."
            redirect_to(:action => "login")
          end
        end
