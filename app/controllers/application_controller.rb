class ApplicationController < ActionController::Base
 # Prevent CSRF attacks by raising an exception.
 # For APIs, you may want to use :null_session instead.
 protect_from_forgery with: :exception
 before_filter :login_status
 protected

  def confirm_logged_in
    unless session[:user_id]
      flash[:notice]="Please log in."
      redirect_to(:controller=>'access', :action => 'login')
      return false
    else
      return true
    end
  end

 def logged_in_as_admin
     unless session[:user_id] && session[:admin_rights]
       flash[:notice]="Sorry - you need to be a super user for that."
       redirect_to(:controller=>'access', :action => 'login')
       return false
     else
       return true
     end
   end

  def login_status
    if session[:user_id] && session[:admin_rights]
       @admin_user="true"
    elsif session[:user_id]
       @regular_user="true"
    end
  end

  def logged_in_as_me
    @ses = session[:user_id].to_s
    @req = @user.id.to_s
    if @ses == @req
    else
      flash[:notice]="These are not the droids you are looking for."
      redirect_to(:controller=>'home', :action => 'index')
    end
  end
  
  def json_request?
    request.format.json?
  end
end