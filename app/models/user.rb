require 'digest/sha1'
          class User < ActiveRecord::Base

            attr_accessor :password



            #EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i

            validates :first_name, :presence => true, :length => { :maximum => 25 }
            validates :last_name, :presence => true, :length => { :maximum => 50 }
            validates :email, :length => { :within => 4..200 }, :uniqueness => true
            validates :password, :presence => true, :length => {:within => 5..25}, :confirmation => true , :on => :create
            validates_confirmation_of :password

            before_save :create_hashed_password
            after_save :clear_password

            def name
              "#{first_name} #{last_name}"
            end

            def self.authenticate(email="", password="")
              user = User.find_by_email(email)
              if user && user.password_match?(password)
                return user
              else
                return false
              end
            end

            # The same password string with the same hash method and salt
            # should always generate the same hashed_password.
            def password_match?(password="")
              hashed_password == User.hash_with_salt(password, salt)
            end

            def self.make_salt(email="")
              Digest::SHA1.hexdigest("Use #{email} with #{Time.now} to make salt")
            end

            def self.hash_with_salt(password="", salt="")
              Digest::SHA1.hexdigest("Put #{salt} on the #{password}")
            end

            private

            def create_hashed_password
              # Whenever :password has a value hashing is needed
              unless password.blank?
                # always use "self" when assigning values
                self.salt = User.make_salt(email) if salt.blank?
                self.hashed_password = User.hash_with_salt(password, salt)
              end
            end

            def clear_password
              # for security and b/c hashing is not needed
              self.password = nil
            end
          end