json.extract! @user, :id, :first_name, :last_name, :email, :is_admin, :password, :hashed_password, :salt, :address1, :address2, :address3, :postcode, :city, :phone_number, :created_at, :updated_at
